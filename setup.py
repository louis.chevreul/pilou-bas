#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 15:02:12 2017

@author: louis
"""

from setuptools import setup
setup(
      author="boule et bill",
      author_email="boule.bill@bys-malraux.net",
      classifiers=[
              'Development Status :: 2 - Pre-Alpha',
              'Environment :: Console',
              'Intended Audience :: End Users/Desktop',
              'License :: OSI Approved :: GNU General Public License v3 or Later (GPLv3+)',
              'Natural Language :: Français',
              'Operating System :: POSIX :: Linux',
              'Programming Language :: Python :: 3',
              'Topic :: Multimedia :: Sound/Audio',
              'Topic :: Utilities'
              ],
      description="Test du déploiement continu",
      entry_points={
              'console_scripts': [
                      'bonjour = prog:run'
                      ]
              },
      install_requires=[],
      keywords='bonjour',
      license="GPLv3",
      long_description="houlala c'est long",
      name="bonjour",
      packages=['prog'],
      python_requires='>=3.2, <4',
      url="http://bts.bts-malraux72.net/~b.bill",
      version="0.1"
)